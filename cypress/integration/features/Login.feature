Feature: Login

    Logs in to the jiosaavn site

    Scenario: Site login
        Given I open jiosaavn login page
        When I type in username and password
        And Click on the sign in button
        Then I must be logged in
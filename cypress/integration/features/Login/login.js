import Homepage from '/Users/aswingokulachandran/cypressWebAuto/cypress/pageObjects/homepage'
import LoginPO from '/Users/aswingokulachandran/cypressWebAuto/cypress/pageObjects/login'

Given(/^I open jiosaavn login page$/, () => {
	cy.visit(Cypress.config().baseUrl);
	const homepage = new Homepage();
	homepage.login().click();
});

const loginPO = new LoginPO();

When(/^I type in username and password$/, () => {
	
	loginPO.email().type(Cypress.env('username'));
	loginPO.password().type(Cypress.env('password'));
	
	//TODO: captcha
	
	
});

When(/^Click on the sign in button$/, () => {
	loginPO.signinButton().click();
});

Then(/^I must be logged in$/, () => {
	return true;
});

import Homepage from '/Users/aswingokulachandran/cypressWebAuto/cypress/pageObjects/homepage'

Given(/^I visit the jiosaavn site$/, () => {
    cy.visit(Cypress.config().baseUrl);
    
});

When(/^I click on the Browse from header$/, () => {
    const homepage = new Homepage();
	homepage.browse().click();
});

Then(/^I should be navigated to the New Releases page$/, () => {
	cy.location().should((loc) => {
        expect(loc.href).to.eq(Cypress.config().baseUrl+'/new-releases');        
    })
});

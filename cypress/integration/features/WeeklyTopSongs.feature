Feature: WeeklyTopSongs
    Navigate to Weekly Top Songs from home and play

    Scenario: Play Weekly Top Songs

    Given I visit the jiosaavn site
    When I locate the Weekly Top Songs tile
    And click on play icon of the Top songs tile
    Then Weekly top songs must be loaded into queue
    
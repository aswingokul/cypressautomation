import Homepage from '/Users/aswingokulachandran/cypressWebAuto/cypress/pageObjects/homepage'



Given(/^I visit the jiosaavn site$/, () => {
	cy.visit(Cypress.config().baseUrl);
});

When(/^I locate the Weekly Top Songs tile$/, () => {
	// cy.scrollTo('center', {duration: 5, easing : "swing"});
	cy.contains('Weekly Top Songs').should('be.visible').click();
	// cy.get('a.u-ellipsis u-color-js-gray').contains('Weekly Top Songs').scrollIntoView().click();
});

When(/^click on play icon of the Top songs tile$/, () => {
	cy.contains('Play').should('be.visible').click();
});

Then(/^Weekly top songs must be loaded into queue$/, () => {

	cy.get('section.u-margin-bottom-large\\@sm>ol').first().find('h4>a.u-color-js-gray').its('text').log();
	// cy.get('aside.c-player').should('be.visible').find('h4>a.u-color-js-gray').its('text');
	

	// const songName = "";
	// let firstElem = cy.get('ol.o-list-bare u-margin-bottom-none@sm').eq(0);
	// firstElem.get('a.u-color-js-gray').then(($el) => {

	// });
});



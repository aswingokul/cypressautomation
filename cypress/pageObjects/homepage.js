class homepage {
    browse(){
        return cy.get('a.c-nav__link[title="Browse"]');
    }

    login(){
        return cy.get('a.c-nav__link[title = "Log In"]');
    }

    signup() {
        return cy.get('a.c-nav__link[title = "Sign Up"]');
    }

    home() {
        return cy.get('a.c-nav__link[title = "Home"]');
    }

    getElementWithText(element, textString) {
        return cy.get(element).contains(textString);
    }
}

export default homepage
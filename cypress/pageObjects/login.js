class login {
    email() {
        return cy.get('#email');
    }

    password() {
        return cy.get('#password');
    }

    signinButton() {
        return cy.get('button.c-btn--wide[text="Continue"]');
    }
}

export default login